<?php

class UtilisateurModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListeUtilisateurs() {

//Requête attendue de type SELECT (liste des lieux)
        $sql = "SELECT * FROM " . "utilisateur LEFT JOIN type_utilisateur ON utilisateur.idTypeUtilisateur=type_utilisateur.idTypeUtilisateur";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnUtilisateur() {

//Requête attendue de type SELECT (un seul utilisateur)
        $sql = "SELECT * FROM " . "utilisateur "
                . "LEFT JOIN type_utilisateur ON utilisateur.idTypeUtilisateur=type_utilisateur.idTypeUtilisateur"
                . " WHERE idUtilisateur = ?";

        $idRequete = $this->executeQuery($sql, array($this->parametre['idUtilisateur']));

//var_dump($idRequete->fetch());
        $user = new UtilisateurTable($idRequete->fetch());

        return $user;
    }

    public function addUtilisateur(UtilisateurTable $valeurs) {
// Requête de type Insert (création)
        $sql = "INSERT INTO " . "utilisateur (nomUtilisateur, prenomUtilisateur, mailUtilisateur, loginUtilisateur,"
                . " motDePasseUtilisateur, idTypeUtilisateur)"
                . " VALUES (?,?,?,?,?,(SELECT type_utilisateur.idTypeUtilisateur FROM type_utilisateur WHERE type_utilisateur.typeUtilisateur = ? ))";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomUtilisateur(),
            $valeurs->getPrenomUtilisateur(),
            $valeurs->getMailUtilisateur(),
            $valeurs->getLoginUtilisateur(),
            $valeurs->getMotDePasseUtilisateur(),
            $valeurs->getTypeUtilisateur()
        ));

        if ($idRequete) {
            UtilisateurTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editUtilisateur(UtilisateurTable $valeurs) {
// Requête de type Insert (création)
        $sql = "UPDATE utilisateur SET nomUtilisateur = ?, prenomUtilisateur = ?, mailUtilisateur = ?,"
                . " loginUtilisateur = ?, motDePasseUtilisateur = ?, "
                . " idTypeUtilisateur = (SELECT type_utilisateur.idTypeUtilisateur FROM type_utilisateur WHERE type_utilisateur.typeUtilisateur = ?)"
                . "WHERE idUtilisateur = ?";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomUtilisateur(),
            $valeurs->getPrenomUtilisateur(),
            $valeurs->getMailUtilisateur(),
            $valeurs->getLoginUtilisateur(),
            $valeurs->getMotDePasseUtilisateur(),
            $valeurs->getTypeUtilisateur(),
            $valeurs->getIdUtilisateur()
        ));
        if ($idRequete) {
            UtilisateurTable::setMessageSucces("Modification effectuée avec succès!");
        }
    }

    public function deleteUtilisateur() {

        $sql = "DELETE FROM utilisateur WHERE idUtilisateur = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idUtilisateur']));

        if ($idRequete) {

            UtilisateurTable::setMessageSucces("Suppression effectuée avec succès!");
        }
    }

}
