<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        {if $role eq 'Administrateur'}
            {include file="public/menu_Administrateur.tpl"}
        {/if}
        {if $role eq 'Modérateur'}
            {include file="public/menu_Moderateur.tpl"}
        {/if}
        {if $role eq 'Utilisateur'}
            {include file="public/menu_Utilisateur.tpl"}
        {/if}
        <div class="container-fluid mt-5">



            <div class="row">

                <div class=" container-fluid text-center">
                    <h3>{$titreGestion}</h3>
                </div>
                <div class="col-md-2 space">

                </div>
            </div>

            <div class="row mt-5">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    <p {if $unUtilisateur->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
                        {$unUtilisateur->getMessageErreur()}
                    </p>
                </div>

            </div>





            <!-- ICI LES DONNES, LE FORMULAIRE (LA FICHE !) -->
            <div class="container col-md-offset-2 col-md-8 col-md-offset-2 space">
                <form action="index.php" method="post" novalidate="">

                    <input type="hidden" name="gestion" value="utilisateur">
                    <input type="hidden" name="action" value="{$action}">
                    <input id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="hidden" value="">

                    {if $action neq 'ajouter'}
                        <div class="form-group">
                            <input class="form-control" id="idUtilisateur" name="idUtilisateur" type="hidden" value="{$unUtilisateur->getIdUtilisateur()}" readonly>
                        </div>
                    {/if}
                    <div class="form-group">
                        Nom <sup>(*)</sup> :
                        <strong>
                            <input class="form-control" id="nomUtilisateur" name="nomUtilisateur" type="text" value="{$unUtilisateur->getNomUtilisateur()}"  {$comportement} required="required">
                        </strong>
                    </div>
                    <div class="form-group">
                        Prenom <sup>(*)</sup> :
                        <strong>
                            <input class="form-control" id="prenomUtilisateur" name="prenomUtilisateur" type="text" value="{$unUtilisateur->getPrenomUtilisateur()}"  {$comportement} required="required">
                        </strong>
                    </div>
                    <div class="form-group">
                        Mail de l'utilisateur <sup>(*)</sup>:
                        <input class="form-control" id="mailUtilisateur" name="mailUtilisateur" type="text" value="{$unUtilisateur->getMailUtilisateur()}" {$comportement} required="required">
                    </div>
                    <div class="form-group">
                        Login <sup>(*)</sup> :
                        <input class="form-control" id="loginUtilisateur" name="loginUtilisateur" type="text" value="{$unUtilisateur->getLoginUtilisateur()}" {$comportement} required="required">
                    </div>
                    <div class="form-group">
                        Mot de passe <sup>(*)</sup> :
                        <input class="form-control" id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="text" value="{$unUtilisateur->getMotDePasseUtilisateur()}" {$comportement} required="required">
                    </div>
                    <div class="form-group">
                        Role:
                        <input class="form-control" id="typeUtilisateur" name="typeUtilisateur" type="text" value="{$unUtilisateur->getTypeUtilisateur()}" {$comportement} required="required">
                    </div>

                    <div class="form-group">

                        <div class="col-md-11">
                            <input type="button"  class="btn btn-warning btn-xs"
                                   onclick='location.href = "index.php?gestion=utilisateur"' value="Retour">
                        </div>

                        {if $action neq 'consulter'}
                            <div class="col-md-1">
                                <input type="submit" class="btn btn-warning btn-sm" value="{$action|capitalize}">
                            </div>
                        {/if}

                    </div>

                </form>

            </div>


            {include file='public/piedPage.tpl'}

        </div>
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/scripts.js"></script>
        <script src="public/js/custom.js" type="text/javascript"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    </body>
</html>
