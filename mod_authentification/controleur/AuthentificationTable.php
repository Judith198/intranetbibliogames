<?php

class AuthentificationTable {

    private $f_login = "";
    private $f_motdepasse = "";
    private $role = "";
    private $autorisationSession = true;
    private static $messageErreur;

    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
            // Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
            // fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
                // Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data = null) {

        if ($data != null) {

            $this->hydrater($data);
        }
    }

    // -------GETTER ---------
    function getF_login() {
        return $this->f_login;
    }

    function getF_motdepasse() {
        return $this->f_motdepasse;
    }

    function getAutorisationSession() {
        return $this->autorisationSession;
    }

    static function getMessageErreur() {
        return self::$messageErreur;
    }

    function getRole() {
        return $this->role;
    }

    // --- SETTER ------
    function setF_login($f_login) {
        if (ctype_space($f_login) || empty($f_login)) {
            self::setMessageErreur("Authentification invalide");
            $this->setAutorisationSession(false);
        }
        $this->f_login = $f_login;
    }

    function setF_motdepasse($f_motdepasse) {
        if (!ctype_space($f_motdepasse) && !empty($f_motdepasse)) {
            $gauche = 'ar30&y%';
            $droite = 'tk!@';
            $this->f_motdepasse = hash('ripemd128', "$gauche$f_motdepasse$droite");
        } else {
            $this->f_motdepasse = "";
        }
    }

    function setRole($role) {
        $this->role = $role;
    }

    function setAutorisationSession($autorisationSession) {
        $this->autorisationSession = $autorisationSession;
    }

    static function setMessageErreur($messageErreur) {
        self::$messageErreur = $messageErreur;
    }

}
