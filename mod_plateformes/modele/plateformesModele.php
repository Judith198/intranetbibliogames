<?php

/**
 * Description of lieuModele
 *
 * @author tvosgiens
 */
class PlateformesModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListePlateformes() {

        //Requête attendue de type SELECT (liste des typejeux)
        $sql = "SELECT * FROM plateformes";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnePlateforme() {

        //Requête attendue de type SELECT (un seul lieu)
        $sql = "SELECT * FROM plateformes WHERE idPlateforme = ?";

        $idRequete = $this->executeQuery($sql, array($this->parametre['idPlateforme']));

        //var_dump($idRequete->fetch());
        $plateformes = new PlateformesTable($idRequete->fetch());

        return $plateformes;
    }

    public function addPlateformes(PlateformesTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "INSERT INTO plateformes (nomPlateforme)
				  VALUES (?)";

        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomPlateforme()
        ));

        if ($idRequete) {
            PlateformesTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editPlateformes(PlateformesTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "UPDATE plateformes SET nomPlateforme = ?
			 WHERE idPlateforme = ?";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomPlateforme(),
            $valeurs->getIdPlateforme()
        ));

        if ($idRequete) {
            PlateformesTable::setMessageSucces("Modification effectuée avec succès !");
        }
    }

    public function deletePlateformes() {

        $sql = "DELETE FROM plateformes WHERE idPlateforme = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idPlateforme']));

        if ($idRequete) {

            PlateformesTable::setMessageSucces("Suppression effectuée avec succès !");
        }
    }

}
