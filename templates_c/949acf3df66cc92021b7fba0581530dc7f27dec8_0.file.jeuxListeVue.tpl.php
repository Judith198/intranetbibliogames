<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-08 12:59:13
  from 'C:\wamp64\www\bibliogames\bibliogames_1\mod_jeux\vue\jeuxListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e8dcaa12ba012_48666806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '949acf3df66cc92021b7fba0581530dc7f27dec8' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames\\bibliogames_1\\mod_jeux\\vue\\jeuxListeVue.tpl',
      1 => 1586350545,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu.tpl' => 1,
  ),
),false)) {
function content_5e8dcaa12ba012_48666806 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>  <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>


        <link href="public/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

            <?php $_smarty_tpl->_subTemplateRender('file:public/menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=jeux"></a>
                </div>
                <div class="col-md-6 space">

                </div>
                <div class="col-md-2 space">



                </div>
            </div>

            					



            <div class="row">
                <!-- ICI LES DONNES  -->
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                    <h1>LISTE DES JEUX</h1>

                    <table class="table">
                        <thead class="">
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Nom du jeu
                                </th>
                                <th>
                                    Type du jeu
                                </th>
                                <th>
                                    PEGI
                                </th>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Année de sortie du jeu
                                </th>
                                <th>
                                    Description
                                </th>
                                <th>
                                    Code d'activation
                                </th>


                            </tr>
                        </thead>

                        <tbody>

                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeJeux']->value, 'jeux');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['jeux']->value) {
?>
                                <tr> 
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['jeux']->value['nomJeux'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['jeux']->value['libelleTypeJeux'];?>

                                    </td>

                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['jeux']->value['agePegi'];?>

                                    </td>

                                    <td>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['jeux']->value['imgJeux'];?>
">
                                    </td>

                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['jeux']->value['anneeSortieJeux'];?>

                                    </td>

                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['jeux']->value['descriptionJeux'];?>

                                    </td>

                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['jeux']->value['codeActivationJeux'];?>

                                    </td>


                                    <td>
                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                            <input type='hidden' name='gestion' value='jeux'>
                                            <input type='hidden' name='action' value='form_consulter'>

                                            <input type="submit"  class="btn btn-warning btn-sm"   name="consulter" value="Consulter">
                                        </form>



                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='gestion' value='jeux'>
                                            <input type='hidden' name='action' value='form_ajouter'>
                                            <input type="submit"  class="btn btn-warning btn-sm"  name="ajouter" value="Ajouter">
                                        </form>



                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                            <input type='hidden' name='gestion' value='jeux'>
                                            <input type='hidden' name='action' value='form_modifier'>

                                            <input type="submit"  class="btn btn-warning btn-sm"   name="modifier" value="Modifier">                                                  
                                        </form>

                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                            <input type='hidden' name='gestion' value='jeux'>
                                            <input type='hidden' name='action' value='form_supprimer'>

                                            <input type="submit"  class="btn btn-warning btn-sm"   name="supprimer" value="Supprimer">                                                          
                                        </form>
                                    </td>
                                </tr>
                                
                            <?php
}
} else {
?>
                                <tr>
                                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>

        <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
