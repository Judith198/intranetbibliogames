<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{$titre|upper}</title>

        <!-- Bootstrap core CSS -->
        <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

        <!-- Custom styles for this template -->
        <!-- <link href="css/small-business.css" rel="stylesheet">-->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >


        <link href="public/css/style.css" rel="stylesheet">



    </head>

    <body>

        <!-- Navigation -->

 
        <div class="container-fluid">

            {include file='public/menu_utilisateur.tpl'}
            
            <div class="marge">

            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=jeux"></a>
                </div>


            </div>

            <!-- Page Content -->
            <div class="container">
                <h1>  Liste des jeux</h1>

                {foreach from=$listeJeux item=jeux}
                    <div class="esp"></div>
                    <!-- Heading Row -->
                    <div class="row align-items-center my-5">
                        <div class="col-lg-7">
 
                            <img  class="img-fluid rounded mb-4 mb-lg-0 " src="{$jeux.imgJeux}"  alt="Image du jeu">
                        </div>
                        <!-- /.col-lg-8 -->
                        <div class="col-lg-5">
                            <h2 class="font-weight-light"><strong>  {$jeux.nomJeux}</strong></h2>
                            <p> {$jeux.descriptionJeux}</p>
                            
                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='{$jeux.idJeux}'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_consulter'>

                                <input type="submit"  class="btn btnVert btn-sm"   name="consulter" value="Consulter">
                            </form>



                            <form action='index.php' method='post'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_ajouter'>
                                <input type="submit"  class="btn btnVert btn-sm"  name="ajouter" value="Ajouter">
                            </form>



                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='{$jeux.idJeux}'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_modifier'>

                                <input type="submit"  class="btn btnVert btn-sm"   name="modifier" value="Modifier">                                                  
                            </form>

                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='{$jeux.idJeux}'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_supprimer'>

                                <input type="submit"  class="btn btnVert btn-sm"   name="supprimer" value="Supprimer">                                                          
                            </form>
                        </div>
                        <!-- /.col-md-4 -->
                    </div>
                    <!-- /.row -->

                {foreachelse}
                    <tr>
                        <td colspan='6'>Aucun enregistrement de trouvé.</td>
                    </tr>
                {/foreach}


            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        </div>

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; BIBLIOGAMES 2020</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <!-- <script src="vendor/jquery/jquery.min.js"></script>
         <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script> 
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script> 
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    </body>

</html>
