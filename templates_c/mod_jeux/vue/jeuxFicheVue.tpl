<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >

        <link href="public/css/style.css" rel="stylesheet">



    </head>
    <body>

        <div class="container-fluid">


            {include file="public/menu_utilisateur.tpl"}




        </div>	
        <div class="espace"></div>

        <div class="flex">
            <div>
                    <form   id="formValidation">     <!--POUR VALIDATION FORM AVEC BIBLIO jQuery fonction validate()-->

                        <div class="form-group">
                            <i class="fas fa-paper-plane  text-primary mb-2"></i>
                            <h4 class="text-uppercase ">Demande code d'activation</h4>
                            <hr  class="traitSousCode" style="border-color:  #C2185B">

                            <div class="form-group ">

                            </div>

                            <input type="text" class="form-control form-control2" placeholder="Nom" id="nom" name="contactNom"  required >
                            <div class="form-group ">

                            </div>
                            <input type="text" class="form-control form-control2" placeholder="Prénom" id="prenom"  name="contactPrenom" required >
                            <div class="form-group ">

                            </div>
                            <input type="email" class="form-control form-control2" placeholder="Adresse mail" id="email"   name="email"  >    
                        </div>

                   

                        <div class="form-group ">
                            <textarea id="story" name="story"  placeholder="Message"
                                      rows="2" cols="32"></textarea>
                        </div>
                       
                        <div class="form-group  couleurLettre">
                            <a id="send-msg" class="btn btnVert2"> Envoi </a>  
                        </div>

                    </form> 

            </div>

            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    {*           <p {if $leLieu->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
                    {$leLieu->getMessageErreur()}
                    </p>
                    </div>    *}





                    <div class="row">
                        <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                        <div class="col-md-offset-2 ">
                            <form action="index.php" method="post" novalidate="">
                                <h1>FICHE JEU</h1>

                                <input type="hidden" name="gestion" value="jeux">
                                <input type="hidden" name="action" value="{$action}">


                                {if $action neq 'ajouter'}
                                    <div class="form-group  ">
                                        <label><strong> Identifiant :</strong> </label>
                                        <input class="form-control" id="idJeux" name="idJeux" type="text" value="{$unJeu->getIdJeux()}" readonly>    {*unJeu dans jeuxVue.php*}
                                    </div>
                                {/if}
                                <div class="form-group inputGroup-sizing-lg">
                                    <label><strong> Nom du jeu  :</strong></label>
                                    <strong> 
                                        <input class="form-control" id="nomJeux" name="nomJeux" type="text" value="{$unJeu->getNomJeux()}"  {$comportement} required="required">
                                    </strong>
                                </div>

                                <div class="form-group ">
                                    <label><strong> Image du jeu :</strong></label>
                                    <img src="{$unJeu->getImgJeux()}" {$comportement} >

                                </div>

                                <div class="form-group">
                                    <label> <strong> Année de sortie du jeu	:</strong></label>
                                    <input class="form-control" id="anneeSortieJeux" name="anneeSortieJeux" type="text" value="{$unJeu->getAnneeSortieJeux()}"  {$comportement}>
                                </div>
                                <div class="form-group ">
                                    <label> <strong>Description du jeu : </strong></label>
                                    <input class="form-control" id="descriptionJeux"   name="descriptionJeux" type="text" value="{$unJeu->getDescriptionJeux()}"  {$comportement}>
                                </div>
                                {*   <div class="form-group">
                                <label>   Code d'activation du jeu : </label>
                                <input class="form-control" id="codeActivationJeux" name="codeActivationJeux" type="text" value="{$unJeu->getCodeActivationJeux()}"  {$comportement}>
                                </div>
                                *}
                                <div class="form-group ">    

                                    <label><strong>  PEGI :</strong></label>
                                    <input class="form-control" id="agePegi"  name="agePegi" type="text" value="{$unJeu->getAgePegi()}"   {$comportement} >
                                </div>
                                <div class="form-group ">

                                    <label><strong>   Type du jeu  :</strong></label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="{$unJeu->getLibelleTypeJeux()}"  {$comportement} >
                                </div>

                                <div class="form-group ">

                                    <label><strong> Plateforme  :</strong></label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="{$unJeu->getNomPlateforme()}"  {$comportement} >
                                </div>

                                <div class="form-group  flexBtn3 ">

                                    <div class="col-sm-10">
                                        <input type="button"  class="btn btnVert btn-sm"
                                               onclick='location.href = "index.php?gestion=jeux"' value="Retour">
                                    </div>

                                    {if $action neq 'consulter'}
                                        <div class="col-md-1">
                                            <input type="submit" class="btn btnVert btn-sm" value="{$action|capitalize}">
                                        </div>
                                    {/if}

                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div> {*ferme div class flex*}
            <script src="public/js/jquery.min.js"></script>
            <script src="public/js/bootstrap.min.js"></script>
            <script src="public/js/scripts.js"></script>

            <script src="public/js/ajaxEnvoiMail.js" type="text/javascript"></script>
            <script src="public/js/jqueryvalidate.js" type="text/javascript"></script>    <!--BIBLIOTHEQUE JQUERY POUR VALIDATION FORMULAIRE -->
            <script src="public/js/formvalidation.js" type="text/javascript"></script>
            <script>

            </script>
    </body>
</html>
