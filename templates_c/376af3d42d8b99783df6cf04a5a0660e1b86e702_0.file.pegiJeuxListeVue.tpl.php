<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-15 04:04:04
  from 'C:\wamp64\www\Projet2Bibliogames\IntranetBibliogames\mod_pegiJeux\vue\pegiJeuxListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9687b4dcd274_59418908',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '376af3d42d8b99783df6cf04a5a0660e1b86e702' => 
    array (
      0 => 'C:\\wamp64\\www\\Projet2Bibliogames\\IntranetBibliogames\\mod_pegiJeux\\vue\\pegiJeuxListeVue.tpl',
      1 => 1586923269,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e9687b4dcd274_59418908 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>  
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
                <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

            <?php $_smarty_tpl->_subTemplateRender('file:public/menu_utilisateur.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            					


            <div class="marge2"> 
                <div class="row">
                    <!-- ICI LES DONNES  -->
                    <div class="col-md-offset-1 col-md-10 col-md-offset-1">


                        <table class="table">
                            <h1>Liste des PEGI</h1>
                            <thead class="">
                                <tr>
                                    <th>
                                        Identifiant du PEGI
                                    </th>
                                    <th>
                                        Pegi jeux
                                    </th>


                                </tr>
                            </thead>

                            <tbody>

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listePegiJeux']->value, 'pegi');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['pegi']->value) {
?>
                                    <tr> 


                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['pegi']->value['idPegi'];?>

                                        </td>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['pegi']->value['agePegi'];?>

                                        </td>


                                        <td>
                                            <form action='index.php' method='post'>
                  

                                                <form action='index.php' method='post'>
                                                    <input type='hidden' name='gestion' value='pegi'>
                                                    <input type='hidden' name='action' value='form_ajouter'>
                                                    <input type="submit"  class="btn btnVert btn-sm"  name="ajouter" value="Ajouter">
                                                </form>
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idPegi' value='<?php echo $_smarty_tpl->tpl_vars['pegi']->value['idPegi'];?>
'>
                                                <input type='hidden' name='gestion' value='pegiJeux'>
                                                <input type='hidden' name='action' value='form_modifier'>

                                                <input type="submit"  class="btn  btnVert  btn-sm"   name="modifier" value="Modifier">                                                  
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idPegi' value='<?php echo $_smarty_tpl->tpl_vars['pegi']->value['idPegi'];?>
'>
                                                <input type='hidden' name='gestion' value='pegiJeux'>
                                                <input type='hidden' name='action' value='form_supprimer'>

                                                <input type="submit"  class="btn  btnVert btn-sm"   name="supprimer" value="Supprimer">                                                          
                                            </form>
                                        </td>
                                    </tr>
                                <?php
}
} else {
?>
                                    <tr>
                                        <td colspan='6'>Aucun enregistrement trouvé.</td>
                                    </tr>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

        <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
