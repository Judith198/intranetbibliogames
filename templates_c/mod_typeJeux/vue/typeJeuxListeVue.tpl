<!DOCTYPE html>  
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
     {*    <link href="public/css/bootstrap.min.css" rel="stylesheet"> *}
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

            {include file='public/menu_utilisateur.tpl'}

            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=typeJeux"></a>
                </div>

            </div>



            <div class="marge3"> 
                <div class="row">
                    <!-- ICI LES DONNES  -->
                    <div class="col-md-offset-1 col-md-10 col-md-offset-1">

                        <h1>Liste des types de jeux</h1>
                        <table class="table">
                            <thead class="">
                                <tr>
                                    <th>
                                        Identifiant 
                                    </th>
                                    <th>
                                        Type jeux
                                    </th>

                                </tr>
                            </thead>

                            <tbody>

                                {foreach from=$listeTypeJeux item=typeJeux}
                                    <tr> 
                                        <td>
                                            {$typeJeux.idTypeJeux}
                                        </td>
                                        <td>
                                            {$typeJeux.libelleTypeJeux}
                                        </td>

                                        <td>
                                    

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='gestion' value='typeJeux'>
                                                <input type='hidden' name='action' value='form_ajouter'>
                                                <input type="submit"  class="btn btnVert btn-sm"  name="ajouter" value="Ajouter">
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idTypeJeux' value='{$typeJeux.idTypeJeux}'>
                                                <input type='hidden' name='gestion' value='typeJeux'>
                                                <input type='hidden' name='action' value='form_modifier'>

                                                <input type="submit"  class="btn  btnVert  btn-sm"   name="modifier" value="Modifier">                                                  
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idTypeJeux' value='{$typeJeux.idTypeJeux}'>
                                                <input type='hidden' name='gestion' value='typeJeux'>
                                                <input type='hidden' name='action' value='form_supprimer'>

                                                <input type="submit"  class="btn  btnVert btn-sm"   name="supprimer" value="Supprimer">                                                          
                                            </form>
                                        </td>
                                    </tr>
                                {foreachelse}
                                    <tr>
                                        <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                    </tr>
                                {/foreach}

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <script src="public/js/scripts.js"></script>
    </body>
</html>
