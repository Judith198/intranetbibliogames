<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-12 16:31:31
  from 'C:\wamp64\www\bibliogames2\mod_utilisateur\vue\utilisateurListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e934263e368e8_90023204',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e2b02b7f90cdc7e36a48bf50d7afd77129394b11' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames2\\mod_utilisateur\\vue\\utilisateurListeVue.tpl',
      1 => 1586709086,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e934263e368e8_90023204 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-0">
    <div class="container-fluid p-1">
        <a class="navbar-brand" href="index.php?gestion=accueil">Accueil</a>


        <div class="input-group d-flex justify-content-end">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Espace personnel
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?gestion=authentification&action=deconnexion"><?php echo $_smarty_tpl->tpl_vars['deconnexion']->value;?>
</a>
                    <a class="dropdown-item" href="index.php?gestion=">Utilisateurs</a>
                </div>
            </div>
        </div>

    </div>
</nav>
        <div class="container-fluid">

            <div class="row mt-5">
                <div class="col-md-4 space">

                </div>
                <div class="col-md-6 space">
                    <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                </div>
                <div class="col-md-2 space">

                    <form action='index.php' method='post'>
                        <input type='hidden' name='gestion' value='utilisateur'>
                        <input type='hidden' name='action' value='form_ajouter'>

                        Ajouter un Utilisateur :
                        <input type="submit"  class="btn btn-primary btn-sm mt-5"  name="ajouter" value="Ajouter">
                    </form>

                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                    
                </div>
            </div>



            <div class="row">
                <!-- ICI LES DONNES  -->
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">


                    <table class="table">
                        <thead class="">
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    NOM
                                </th>
                                <th>
                                    PRENOM
                                </th>
                                <th>
                                    MAIL
                                </th>
                                <th>
                                    LOGIN
                                </th>

                                <th>
                                    ROLE
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                                                    </td>
                            </tr>

                        </tfoot>
                        <tbody>

                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeUtilisateurs']->value, 'utilisateur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['utilisateur']->value) {
?>
                                <tr>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['nomUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['prenomUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['mailUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['loginUtilisateur'];?>

                                    </td>

                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['typeUtilisateur'];?>

                                    </td>

                                    <td>
                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='<?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>
'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_consulter'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="consulter" value="Consulter">
                                        </form>

                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='<?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>
'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_modifier'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="modifier" value="Modifier">
                                        </form>

                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='<?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>
'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_supprimer'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="supprimer" value="Supprimer">
                                        </form>
                                    </td>
                                </tr>
                            <?php
}
} else {
?>
                                <tr>
                                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        </tbody>
                    </table>
                </div>
            </div>
                            
<footer class="bg-dark fixed-bottom mx-0">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Bibliogames 2020</p>
    </div>
    <!-- /.container -->
</footer>

        </div>

        <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
