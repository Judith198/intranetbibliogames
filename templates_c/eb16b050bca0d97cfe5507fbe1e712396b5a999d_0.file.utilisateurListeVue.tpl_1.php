<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-13 13:49:58
  from 'C:\wamp64\www\bibliogames\mod_utilisateur\vue\utilisateurListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e946e06180ea2_56591972',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eb16b050bca0d97cfe5507fbe1e712396b5a999d' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames\\mod_utilisateur\\vue\\utilisateurListeVue.tpl',
      1 => 1586785690,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_Administrateur.tpl' => 1,
    'file:public/menu_Moderateur.tpl' => 1,
    'file:public/menu_Utilisateur.tpl' => 1,
    'file:public/piedPage.tpl' => 1,
  ),
),false)) {
function content_5e946e06180ea2_56591972 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Administrateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Moderateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Utilisateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Utilisateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <div class="container-fluid">

            <div class="row mt-5">
                <div class="col-md-4 space">

                </div>
                <div class="col-md-6 space">
                    <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                </div>
                <div class="col-md-2 space">

                    <form action='index.php' method='post'>
                        <input type='hidden' name='gestion' value='utilisateur'>
                        <input type='hidden' name='action' value='form_ajouter'>

                        Ajouter un Utilisateur :
                        <input type="submit"  class="btn btn-primary btn-sm mt-5"  name="ajouter" value="Ajouter">
                    </form>

                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                    <p>  <?php if ($_smarty_tpl->tpl_vars['message']->value != '') {?> <?php echo $_smarty_tpl->tpl_vars['message']->value;
}?>

                    </p>

                </div>
            </div>



            <div class="row">
                <!-- ICI LES DONNES  -->
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">


                    <table class="table">
                        <thead class="">
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    NOM
                                </th>
                                <th>
                                    PRENOM
                                </th>
                                <th>
                                    MAIL
                                </th>
                                <th>
                                    LOGIN
                                </th>

                                <th>
                                    ROLE
                                </th>
                                <th>
                                    ACTIONS
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                                                    </td>
                            </tr>

                        </tfoot>
                        <tbody>

                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeUtilisateurs']->value, 'utilisateur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['utilisateur']->value) {
?>
                                <tr>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['nomUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['prenomUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['mailUtilisateur'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['loginUtilisateur'];?>

                                    </td>

                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['typeUtilisateur'];?>

                                    </td>

                                    <td>
                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='<?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>
'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_consulter'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="consulter" value="Consulter">
                                        </form>

                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='<?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>
'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_modifier'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="modifier" value="Modifier">
                                        </form>

                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idUtilisateur' value='<?php echo $_smarty_tpl->tpl_vars['utilisateur']->value['idUtilisateur'];?>
'>
                                            <input type='hidden' name='gestion' value='utilisateur'>
                                            <input type='hidden' name='action' value='form_supprimer'>

                                            <input type="submit"  class="btn btn-primary btn-sm rounded mb-1"   name="supprimer" value="Supprimer">
                                        </form>
                                    </td>
                                </tr>
                            <?php
}
} else {
?>
                                <tr>
                                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        </tbody>
                    </table>
                </div>
            </div>

            <?php $_smarty_tpl->_subTemplateRender('file:public/piedPage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>

        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"><?php echo '</script'; ?>
>


    </body>
</html>
<?php }
}
