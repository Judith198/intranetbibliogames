<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-13 05:51:01
  from 'C:\wamp64\www\bibliogames2\mod_typeJeux\vue\typeJeuxFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e93fdc565e1e1_94434814',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '48004737b90f0652fe0edfcacb48574bc985cea1' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames2\\mod_typeJeux\\vue\\typeJeuxFicheVue.tpl',
      1 => 1586757027,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e93fdc565e1e1_94434814 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\bibliogames2\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

   

           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
  
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

     	<?php $_smarty_tpl->_subTemplateRender('file:public/menu_utilisateur.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

     
        <div class="marge2">
            		<div class="row">
    
            <div class="col-md-offset-2 col-md-8 col-md-offset-2">

 
            </div>	 					




            <div class="row">
                <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                <div class="col-md-offset-2 col-md-8 col-md-offset-2 space">
                    <form action="index.php" method="post" novalidate="">
                           <h1>FICHE TYPE JEU</h1>

                        <input type="hidden" name="gestion" value="typeJeux">
                        <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">


                        <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                            <div class="form-group ">
                                <label> Identifiant : </label>
                                <input class="form-control" id="idTypeJeux" name="idTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unTypeJeux']->value->getIdTypeJeux();?>
" readonly>                                </div>
                        <?php }?>
                        <div class="form-group inputGroup-sizing-lg">
                            <label> Type du jeu :</label>
                            <strong> 
                                <input class="form-control" id="libelleTypeJeux" name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unTypeJeux']->value->getlibelleTypeJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                            </strong>
                        </div>

                      

                        <div class="form-group flexBtn4">

                            <div class="col-sm-12">
                                <input type="button"  class="btn btnVert  "
                                       onclick='location.href = "index.php?gestion=typeJeux"' value="Retour">
                            </div>

                            <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                <div class="col-md-1">
                                    <input type="submit" class="btn btnVert" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                </div>
                            <?php }?>

                        </div>

                    </form>

                </div>
            </div>
                        </div>
        </div>
        </div>

        <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
