<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-15 04:56:29
  from 'C:\wamp64\www\Projet2Bibliogames\IntranetBibliogames\mod_jeux\vue\jeuxFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9693fd7bd292_65283125',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b92338bd0a56ef1affbc903ab4aad6000bba85eb' => 
    array (
      0 => 'C:\\wamp64\\www\\Projet2Bibliogames\\IntranetBibliogames\\mod_jeux\\vue\\jeuxFicheVue.tpl',
      1 => 1586924702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e9693fd7bd292_65283125 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\Projet2Bibliogames\\IntranetBibliogames\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >

        <link href="public/css/style.css" rel="stylesheet">



    </head>
    <body>

        <div class="container-fluid">


            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_utilisateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>




        </div>	
        <div class="espace"></div>

        <div class="flex">
            <div>
                    <form   id="formValidation">     <!--POUR VALIDATION FORM AVEC BIBLIO jQuery fonction validate()-->

                        <div class="form-group">
                            <i class="fas fa-paper-plane  text-primary mb-2"></i>
                            <h4 class="text-uppercase ">Demande code d'activation</h4>
                            <hr  class="traitSousCode" style="border-color:  #C2185B">

                            <div class="form-group ">

                            </div>

                            <input type="text" class="form-control form-control2" placeholder="Nom" id="nom" name="contactNom"  required >
                            <div class="form-group ">

                            </div>
                            <input type="text" class="form-control form-control2" placeholder="Prénom" id="prenom"  name="contactPrenom" required >
                            <div class="form-group ">

                            </div>
                            <input type="email" class="form-control form-control2" placeholder="Adresse mail" id="email"   name="email"  >    
                        </div>

                   

                        <div class="form-group ">
                            <textarea id="story" name="story"  placeholder="Message"
                                      rows="2" cols="32"></textarea>
                        </div>
                       
                        <div class="form-group  couleurLettre">
                            <a id="send-msg" class="btn btnVert2"> Envoi </a>  
                        </div>

                    </form> 

            </div>

            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    




                    <div class="row">
                        <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                        <div class="col-md-offset-2 ">
                            <form action="index.php" method="post" novalidate="">
                                <h1>FICHE JEU</h1>

                                <input type="hidden" name="gestion" value="jeux">
                                <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">


                                <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                                    <div class="form-group  ">
                                        <label><strong> Identifiant :</strong> </label>
                                        <input class="form-control" id="idJeux" name="idJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getIdJeux();?>
" readonly>                                        </div>
                                <?php }?>
                                <div class="form-group inputGroup-sizing-lg">
                                    <label><strong> Nom du jeu  :</strong></label>
                                    <strong> 
                                        <input class="form-control" id="nomJeux" name="nomJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getNomJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                                    </strong>
                                </div>

                                <div class="form-group ">
                                    <label><strong> Image du jeu :</strong></label>
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getImgJeux();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >

                                </div>

                                <div class="form-group">
                                    <label> <strong> Année de sortie du jeu	:</strong></label>
                                    <input class="form-control" id="anneeSortieJeux" name="anneeSortieJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getAnneeSortieJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
>
                                </div>
                                <div class="form-group ">
                                    <label> <strong>Description du jeu : </strong></label>
                                    <input class="form-control" id="descriptionJeux"   name="descriptionJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getDescriptionJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
>
                                </div>
                                                                <div class="form-group ">    

                                    <label><strong>  PEGI :</strong></label>
                                    <input class="form-control" id="agePegi"  name="agePegi" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getAgePegi();?>
"   <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>
                                <div class="form-group ">

                                    <label><strong>   Type du jeu  :</strong></label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getLibelleTypeJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>

                                <div class="form-group ">

                                    <label><strong> Plateforme  :</strong></label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getNomPlateforme();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>

                                <div class="form-group  flexBtn3 ">

                                    <div class="col-sm-10">
                                        <input type="button"  class="btn btnVert btn-sm"
                                               onclick='location.href = "index.php?gestion=jeux"' value="Retour">
                                    </div>

                                    <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                        <div class="col-md-1">
                                            <input type="submit" class="btn btnVert btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                        </div>
                                    <?php }?>

                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>             <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>

            <?php echo '<script'; ?>
 src="public/js/ajaxEnvoiMail.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="public/js/jqueryvalidate.js" type="text/javascript"><?php echo '</script'; ?>
>    <!--BIBLIOTHEQUE JQUERY POUR VALIDATION FORMULAIRE -->
            <?php echo '<script'; ?>
 src="public/js/formvalidation.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
>

            <?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
