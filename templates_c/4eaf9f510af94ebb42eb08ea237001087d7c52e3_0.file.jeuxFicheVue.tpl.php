<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-12 10:21:59
  from 'C:\wamp64\www\bibliogames\bibliogames\mod_jeux\vue\jeuxFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e92ebc74ea5e2_99489243',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4eaf9f510af94ebb42eb08ea237001087d7c52e3' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames\\bibliogames\\mod_jeux\\vue\\jeuxFicheVue.tpl',
      1 => 1586686572,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu.tpl' => 1,
  ),
),false)) {
function content_5e92ebc74ea5e2_99489243 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\bibliogames\\bibliogames\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

            <?php $_smarty_tpl->_subTemplateRender('file:public/menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <div class="row">
                <div class="col-md-4 space">

                </div>

                <div class="col-md-4 space">

                </div>

                <div class="col-md-2 space">

                </div>
            </div>

      

                <div class="row">

                    <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                        
                    </div>	



                    <div class="row">
                        <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                        <div class="col-md-offset-2 col-md-8 col-md-offset-2 space">
                            <form action="index.php" method="post" novalidate="">
                                <h1>FICHE JEU</h1>

                                <input type="hidden" name="gestion" value="jeux">
                                <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">


                                <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                                    <div class="form-group ">
                                        <label> Identifiant : </label>
                                        <input class="form-control" id="idJeux" name="idJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getIdJeux();?>
" readonly>                                        </div>
                                <?php }?>
                                <div class="form-group inputGroup-sizing-lg">
                                    <label> Nom du jeu  :</label>
                                    <strong> 
                                        <input class="form-control" id="nomJeux" name="nomJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getNomJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                                    </strong>
                                </div>

                                <div class="form-group">
                                    <label> Image du jeu :</label>
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getImgJeux();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >

                                </div>

                                <div class="form-group">
                                    <label>  Année de sortie du jeu	:</label>
                                    <input class="form-control" id="anneeSortieJeux" name="anneeSortieJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getAnneeSortieJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
>
                                </div>
                                <div class="form-group">
                                    <label> Description du jeu : </label>
                                    <input class="form-control" id="descriptionJeux"   name="descriptionJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getDescriptionJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
>
                                </div>
                                                                <div class="form-group">    

                                    <label>  PEGI :</label>
                                    <input class="form-control" id="agePegi"  name="agePegi" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getAgePegi();?>
"   <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>
                                <div class="form-group">

                                    <label>   Type du jeu  :</label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getLibelleTypeJeux();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>

                                <div class="form-group">

                                    <label> Plateforme  :</label>
                                    <input class="form-control" id="libelleTypeJeux"  name="libelleTypeJeux" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unJeu']->value->getNomPlateforme();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                </div>

                                <div class="form-group">

                                    <div class="col-sm-10">
                                        <input type="button"  class="btn btnVert btn-sm"
                                               onclick='location.href = "index.php?gestion=jeux"' value="Retour">
                                    </div>

                                    <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                        <div class="col-md-1">
                                            <input type="submit" class="btn btnVert btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                        </div>
                                    <?php }?>

                                </div>

                            </form>

                        </div>
                    </div>

                </div>
         
            <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>

            <?php echo '<script'; ?>
 src="public/js/ajaxEnvoiMail.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="public/js/jqueryvalidate.js" type="text/javascript"><?php echo '</script'; ?>
>    <!--BIBLIOTHEQUE JQUERY POUR VALIDATION FORMULAIRE -->
            <?php echo '<script'; ?>
 src="public/js/formvalidation.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
>

            <?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
