<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-12 16:32:16
  from 'C:\wamp64\www\bibliogames2\mod_utilisateur\vue\utilisateurFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e934290e45547_02801063',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e1970d58a1def5f8cdfb1f3a5922508f7467124e' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames2\\mod_utilisateur\\vue\\utilisateurFicheVue.tpl',
      1 => 1586709132,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e934290e45547_02801063 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\bibliogames2\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
  
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-0">
    <div class="container-fluid p-1">
        <a class="navbar-brand" href="index.php?gestion=accueil">Accueil</a>
        
        <div class="input-group d-flex justify-content-end">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Espace personnel
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?gestion=authentification&action=deconnexion"><?php echo $_smarty_tpl->tpl_vars['deconnexion']->value;?>
</a>
                    <a class="dropdown-item" href="index.php?gestion=utilisateur">Utilisateurs</a>
                </div>
            </div>
        </div>

    </div>
</nav>
        <div class="container-fluid">



            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=utilisateur"><img src="public/images/plogo.PNG" ></a>
                </div>
                <div class="col-md-6 space">
                    <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                </div>
                <div class="col-md-2 space">

                </div>
            </div>

            <div class="row mt-5">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    <p <?php if ($_smarty_tpl->tpl_vars['unUtilisateur']->value->getMessageErreur() != '') {?> class="pos-messageErreur" <?php }?>>
                        <?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getMessageErreur();?>

                    </p>
                </div>

            </div>




            <div class="row">
                <!-- ICI LES DONNES, LE FORMULAIRE (LA FICHE !) -->
                <div class="col-md-offset-2 col-md-8 col-md-offset-2 space">
                    <form action="index.php" method="post" novalidate="">

                        <input type="hidden" name="gestion" value="utilisateur">
                        <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                        <input id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="hidden" value="">

                        <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                            <div class="form-group">
                                Identifiant :
                                <input class="form-control" id="idUtilisateur" name="idUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getIdUtilisateur();?>
" readonly>
                            </div>
                        <?php }?>
                        <div class="form-group">
                            Nom <sup>(*)</sup> :
                            <strong>
                                <input class="form-control" id="nomUtilisateur" name="nomUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getNomUtilisateur();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                            </strong>
                        </div>
                        <div class="form-group">
                            Prenom <sup>(*)</sup> :
                            <strong>
                                <input class="form-control" id="prenomUtilisateur" name="prenomUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getPrenomUtilisateur();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                            </strong>
                        </div>
                        <div class="form-group">
                            Mail de l'utilisateur <sup>(*)</sup>:
                            <input class="form-control" id="mailUtilisateur" name="mailUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getMailUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                        </div>
                        <div class="form-group">
                            Login <sup>(*)</sup> :
                            <input class="form-control" id="loginUtilisateur" name="loginUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getLoginUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                        </div>
                        <div class="form-group">
                            Mot de passe <sup>(*)</sup> :
                            <input class="form-control" id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getMotDePasseUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                        </div>
                        <div class="form-group">
                            IdRole <sup>(*)</sup> :
                            <input class="form-control" id="idTypeUtilisateur" name="idTypeUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getIdTypeUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                            <button type="button" class="btn btn-primary btn-sm rounded-circle" data-toggle="tooltip" data-placement="bottom" title="1 = utilisateur / 2 = modérateur / 3 = administrateur">
                                info
                            </button>
                        </div>





                        <div class="form-group">

                            <div class="col-md-11">
                                <input type="button"  class="btn btn-warning btn-xs"
                                       onclick='location.href = "index.php?gestion=utilisateur"' value="Retour">
                            </div>

                            <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                <div class="col-md-1">
                                    <input type="submit" class="btn btn-warning btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                </div>
                            <?php }?>

                        </div>

                    </form>

                </div>
            </div>

     <footer class="bg-dark fixed-bottom mx-0">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Bibliogames 2020</p>
    </div>
    <!-- /.container -->
</footer>

        </div>

        <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/custom.js" type="text/javascript"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
