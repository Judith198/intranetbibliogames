<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
  
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-0">
    <div class="container-fluid p-1">
        <a class="navbar-brand" href="index.php?gestion=accueil">Accueil</a>
        {* <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
        <a class="nav-link" href="#">Home
        <span class="sr-only">(current)</span>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">About</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">Services</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
        </li>

        </ul>
        </div>
        *}

        <div class="input-group d-flex justify-content-end">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Espace personnel
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?gestion=authentification&action=deconnexion">{$deconnexion}</a>
                    <a class="dropdown-item" href="index.php?gestion=utilisateur">Utilisateurs</a>
                </div>
            </div>
        </div>

    </div>
</nav>
        <div class="container-fluid">



            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=utilisateur"><img src="public/images/plogo.PNG" ></a>
                </div>
                <div class="col-md-6 space">
                    <h3>{$titreGestion}</h3>
                </div>
                <div class="col-md-2 space">

                </div>
            </div>

            <div class="row mt-5">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    <p {if $unUtilisateur->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
                        {$unUtilisateur->getMessageErreur()}
                    </p>
                </div>

            </div>




            <div class="row">
                <!-- ICI LES DONNES, LE FORMULAIRE (LA FICHE !) -->
                <div class="col-md-offset-2 col-md-8 col-md-offset-2 space">
                    <form action="index.php" method="post" novalidate="">

                        <input type="hidden" name="gestion" value="utilisateur">
                        <input type="hidden" name="action" value="{$action}">
                        <input id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="hidden" value="">

                        {if $action neq 'ajouter'}
                            <div class="form-group">
                                Identifiant :
                                <input class="form-control" id="idUtilisateur" name="idUtilisateur" type="text" value="{$unUtilisateur->getIdUtilisateur()}" readonly>
                            </div>
                        {/if}
                        <div class="form-group">
                            Nom <sup>(*)</sup> :
                            <strong>
                                <input class="form-control" id="nomUtilisateur" name="nomUtilisateur" type="text" value="{$unUtilisateur->getNomUtilisateur()}"  {$comportement} required="required">
                            </strong>
                        </div>
                        <div class="form-group">
                            Prenom <sup>(*)</sup> :
                            <strong>
                                <input class="form-control" id="prenomUtilisateur" name="prenomUtilisateur" type="text" value="{$unUtilisateur->getPrenomUtilisateur()}"  {$comportement} required="required">
                            </strong>
                        </div>
                        <div class="form-group">
                            Mail de l'utilisateur <sup>(*)</sup>:
                            <input class="form-control" id="mailUtilisateur" name="mailUtilisateur" type="text" value="{$unUtilisateur->getMailUtilisateur()}" {$comportement} required="required">
                        </div>
                        <div class="form-group">
                            Login <sup>(*)</sup> :
                            <input class="form-control" id="loginUtilisateur" name="loginUtilisateur" type="text" value="{$unUtilisateur->getLoginUtilisateur()}" {$comportement} required="required">
                        </div>
                        <div class="form-group">
                            Mot de passe <sup>(*)</sup> :
                            <input class="form-control" id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="text" value="{$unUtilisateur->getMotDePasseUtilisateur()}" {$comportement} required="required">
                        </div>
                        <div class="form-group">
                            IdRole <sup>(*)</sup> :
                            <input class="form-control" id="idTypeUtilisateur" name="idTypeUtilisateur" type="text" value="{$unUtilisateur->getIdTypeUtilisateur()}" {$comportement} required="required">
                            <button type="button" class="btn btn-primary btn-sm rounded-circle" data-toggle="tooltip" data-placement="bottom" title="1 = utilisateur / 2 = modérateur / 3 = administrateur">
                                info
                            </button>
                        </div>





                        <div class="form-group">

                            <div class="col-md-11">
                                <input type="button"  class="btn btn-warning btn-xs"
                                       onclick='location.href = "index.php?gestion=utilisateur"' value="Retour">
                            </div>

                            {if $action neq 'consulter'}
                                <div class="col-md-1">
                                    <input type="submit" class="btn btn-warning btn-sm" value="{$action|capitalize}">
                                </div>
                            {/if}

                        </div>

                    </form>

                </div>
            </div>

     <footer class="bg-dark fixed-bottom mx-0">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Bibliogames 2020</p>
    </div>
    <!-- /.container -->
</footer>

        </div>

        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <script src="public/js/scripts.js"></script>
        <script src="public/js/custom.js" type="text/javascript"></script>
    </body>
</html>
