<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of utilisateurTable
 *
 * @author jorda
 */
class UtilisateurTable {

    private $idUtilisateur = "";
    private $nomUtilisateur = "";
    private $prenomUtilisateur = "";
    private $mailUtilisateur = "";
    private $loginUtilisateur = "";
    private $motDePasseUtilisateur = "bibliogames";
    private $idTypeUtilisateur = "";
    private $typeUtilisateur = "";
    private $autorisationBD = true;
    private static $messageErreur = "";
    private static $messageSucces = "";

// 2 Importer la méthode hydrater
    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
// Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
// fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
// Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data = null) {

        if ($data != null) {

            $this->hydrater($data);
        }
    }

// 3 Getters + Setters : ALT + INSERT
// =========== GETTERS =================

    function getIdUtilisateur() {
        return $this->idUtilisateur;
    }

    function getNomUtilisateur() {
        return $this->nomUtilisateur;
    }

    function getPrenomUtilisateur() {
        return $this->prenomUtilisateur;
    }

    function getMailUtilisateur() {
        return $this->mailUtilisateur;
    }

    function getLoginUtilisateur() {
        return $this->loginUtilisateur;
    }

    function getMotDePasseUtilisateur() {
        return $this->motDePasseUtilisateur;
    }

    function getTypeUtilisateur() {
        return $this->typeUtilisateur;
    }

    function getIdTypeUtilisateur() {
        return $this->idTypeUtilisateur;
    }

// =========== SETTERS =================
    function setIdUtilisateur($idUtilisateur) {

        $this->idUtilisateur = $idUtilisateur;
    }

    function setNomUtilisateur($nomUtilisateur) {

        if (!is_string($nomUtilisateur) || ctype_space($nomUtilisateur) || empty($nomUtilisateur)) {
            self::setMessageErreur("Le nom est invalide");
            $this->setAutorisationBD(false);
        }

        $this->nomUtilisateur = $nomUtilisateur;
    }

    function setPrenomUtilisateur($prenomUtilisateur) {

        if (!is_string($prenomUtilisateur) || ctype_space($prenomUtilisateur) || empty($prenomUtilisateur)) {
            self::setMessageErreur("Le prénom est invalide");
            $this->setAutorisationBD(false);
        }

        $this->prenomUtilisateur = $prenomUtilisateur;
    }

    function setMailUtilisateur($mailUtilisateur) {

        if (!is_string($mailUtilisateur) || ctype_space($mailUtilisateur) || empty($mailUtilisateur)) {
            self::setMessageErreur("L'adresse mail semble invalide");
            $this->setAutorisationBD(false);
        }

        $this->mailUtilisateur = $mailUtilisateur;
    }

    function setLoginUtilisateur($loginUtilisateur) {

        if (!is_string($loginUtilisateur) || ctype_space($loginUtilisateur) || empty($loginUtilisateur)) {
            self::setMessageErreur("Le login est invalide");
            $this->setAutorisationBD(false);
        }

        $this->loginUtilisateur = $loginUtilisateur;
    }

    function setMotDePasseUtilisateur() {
        $pw = $this->motDePasseUtilisateur;
// technique du salage
        $gauche = 'ar30&y%';
        $droite = 'tk!@';

        $this->motDePasseUtilisateur = hash('ripemd128', "$gauche$pw$droite");
    }

    function setIdTypeUtilisateur($idTypeUtilisateur) {
        $this->idTypeUtilisateur = $idTypeUtilisateur;
    }

    function setTypeUtilisateur($typeUtilisateur) {
        $this->typeUtilisateur = $typeUtilisateur;
    }

    /*     * *************AutorisationBD****************** */

    function getAutorisationBD() {
        return $this->autorisationBD;
    }

    function setAutorisationBD($autorisationBD) {
        $this->autorisationBD = $autorisationBD;
    }

    /*     * ********getMessageErreur ou getMessageSucces**************************** */

    public static function getMessageErreur() {
        return self::$messageErreur;
    }

    public static function getMessageSucces() {
        return self::$messageSucces;
    }

    public static function setMessageErreur($msg) {
        self::$messageErreur = self::$messageErreur . $msg . "<br>";
    }

    public static function setMessageSucces($msg) {
        self::$messageSucces = self::$messageSucces . $msg . "<br>";
    }

}
