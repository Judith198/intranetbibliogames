<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-05 15:08:52
  from 'C:\wamp64\www\bibliogames\bibliogames\mod_typeJeux\vue\typeJeuxListeVue1.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e89f4843f5a43_82632281',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7523baee5133e1009685b84d629ee9341953bf36' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames\\bibliogames\\mod_typeJeux\\vue\\typeJeuxListeVue1.tpl',
      1 => 1586099329,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu.tpl' => 1,
  ),
),false)) {
function content_5e89f4843f5a43_82632281 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>  <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

		
		<link href="public/css/bootstrap.min.css" rel="stylesheet">
		<link href="public/css/style.css" rel="stylesheet">

	</head>
	<body>

		<div class="container-fluid">
			
			<?php $_smarty_tpl->_subTemplateRender('file:public/menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
			
			<div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=typeJeux"></a>
                </div>
                <div class="col-md-6 space">
                  
                </div>
                <div class="col-md-2 space">

                 

                </div>
            </div>

						



			<div class="row">
				<!-- ICI LES DONNES  -->
				<div class="col-md-offset-1 col-md-10 col-md-offset-1">


					<table class="table">
						<thead class="">
							<tr>
								<th>
									ID
								</th>
								<th>
									Type jeux
								</th>
								
							</tr>
						</thead>
					
						<tbody>
							
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeTypeJeux']->value, 'typeJeux');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['typeJeux']->value) {
?>
								<tr> 
									<td>
										<?php echo $_smarty_tpl->tpl_vars['typeJeux']->value['idTypeJeux'];?>

									</td>
									<td>
										<?php echo $_smarty_tpl->tpl_vars['typeJeux']->value['libelleTypeJeux'];?>

									</td>
			
								</tr>
							<?php
}
} else {
?>
								<tr>
									<td colspan='6'>Aucun enregistrement de trouvé.</td>
								</tr>
							<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

						</tbody>
					</table>
				</div>
			</div>

	
		
		</div>

		<?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
	</body>
</html>
<?php }
}
