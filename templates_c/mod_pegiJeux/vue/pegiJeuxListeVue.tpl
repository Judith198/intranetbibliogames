<!DOCTYPE html>  
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
        {*   <link href="public/css/bootstrap.min.css" rel="stylesheet">  *}
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

            {include file='public/menu_utilisateur.tpl'}

            {*		<div class="row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <p {if $message neq ''} class='pos-message'{/if}>
            {$message}
            </p>

            </div>
            </div>		*}					


            <div class="marge2"> 
                <div class="row">
                    <!-- ICI LES DONNES  -->
                    <div class="col-md-offset-1 col-md-10 col-md-offset-1">


                        <table class="table">
                            <h1>Liste des PEGI</h1>
                            <thead class="">
                                <tr>
                                    <th>
                                        Identifiant du PEGI
                                    </th>
                                    <th>
                                        Pegi jeux
                                    </th>


                                </tr>
                            </thead>

                            <tbody>

                                {foreach from=$listePegiJeux item=pegi}
                                    <tr> 


                                        <td>
                                            {$pegi.idPegi}
                                        </td>
                                        <td>
                                            {$pegi.agePegi}
                                        </td>


                                        <td>
                                            <form action='index.php' method='post'>
                  

                                                <form action='index.php' method='post'>
                                                    <input type='hidden' name='gestion' value='pegi'>
                                                    <input type='hidden' name='action' value='form_ajouter'>
                                                    <input type="submit"  class="btn btnVert btn-sm"  name="ajouter" value="Ajouter">
                                                </form>
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idPegi' value='{$pegi.idPegi}'>
                                                <input type='hidden' name='gestion' value='pegiJeux'>
                                                <input type='hidden' name='action' value='form_modifier'>

                                                <input type="submit"  class="btn  btnVert  btn-sm"   name="modifier" value="Modifier">                                                  
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idPegi' value='{$pegi.idPegi}'>
                                                <input type='hidden' name='gestion' value='pegiJeux'>
                                                <input type='hidden' name='action' value='form_supprimer'>

                                                <input type="submit"  class="btn  btnVert btn-sm"   name="supprimer" value="Supprimer">                                                          
                                            </form>
                                        </td>
                                    </tr>
                                {foreachelse}
                                    <tr>
                                        <td colspan='6'>Aucun enregistrement trouvé.</td>
                                    </tr>
                                {/foreach}

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <script src="public/js/scripts.js"></script>
    </body>
</html>
