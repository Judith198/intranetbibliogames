<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

   

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

       	{include file='public/menu_utilisateur.tpl'}

            		<div class="row">
    
            <div class="col-md-offset-2 col-md-8 col-md-offset-2">

 {*           <p {if $lePegiJeux->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
            {$lePegiJeux->getMessageErreur()}
            </p>
            </div>    *}

            </div>	 					



            <div class="marge2">
            <div class="row">
                <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                <div class="col-md-offset-2 col-md-8 col-md-offset-2 space">
                    <form action="index.php" method="post" novalidate="">
                           <h1>FICHE PEGI</h1>

                        <input type="hidden" name="gestion" value="pegiJeux">
                        <input type="hidden" name="action" value="{$action}">


                        {if $action neq 'ajouter'}
                            <div class="form-group ">
                                <label> Identifiant Pegi : </label>
                                <input class="form-control" id="idPegi" name="idPegi" type="text" value="{$lePegiJeux->getIdPegi()}" readonly>    {*unJeu dans jeuxVue.php*}
                            </div>
                        {/if}
                        <div class="form-group inputGroup-sizing-lg">
                            <label> Pegi :</label>
                            <strong> 
                                <input class="form-control" id="agePegi" name="agePegi" type="text" value="{$lePegiJeux->getAgePegi()}"  {$comportement} required="required">
                            </strong>
                        </div>

                      

                        <div class="form-group flexBtn2">

                            <div class="col-sm-10">
                                <input type="button"  class="btn btnVert btn-sm"
                                       onclick='location.href = "index.php?gestion=pegiJeux"' value="Retour">
                            </div>

                            {if $action neq 'consulter'}
                                <div class="col-md-1">
                                    <input type="submit" class="btn btnVert btn-sm" value="{$action|capitalize}">
                                </div>
                            {/if}

                        </div>

                    </form>

                </div>
            </div>

        </div>
                        </div>
        </div>

        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
        <script src="public/js/scripts.js"></script>
    </body>
</html>
