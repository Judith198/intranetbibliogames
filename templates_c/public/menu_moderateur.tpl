<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-0">
    <div class="container-fluid p-1">
        <a class="navbar-brand" href="index.php?gestion=accueil">Accueil</a>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                 
                    <li>
                        <a class="nav-color" href="index.php?gestion="></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav-color" data-toggle="dropdown"><strong class="caret">Recherche</strong></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?gestion=jeux">Jeux</a>
                            </li>

                            <li class="divider"> </li>

                            <li>
                                <a href="index.php?gestion=typeJeux">Type de jeux</a>
                            </li>

                            <li class="divider"> </li>


                            <li>
                                <a href="index.php?gestion=plateforme">Plateformes</a>
                            </li>

                            <li class="divider"> </li>
                            <li>
                                <a href="index.php?gestion=pegiJeux">PEGI</a>
                            </li>

                        </ul>
                    </li>
                </ul>

        <div class="input-group d-flex justify-content-end">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Espace personnel
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?gestion=authentification&action=deconnexion">{$deconnexion}</a>
                </div>
            </div>
        </div>

    </div>
</nav>