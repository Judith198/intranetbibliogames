'usestrict';
let tab = ["Welcome !", "Bienvenido !", "Willkommen !", "Benvenuto !", "Bem-vindo !"];
let count = 0;

setInterval(function () {
    count++;
    $("#sliderWord").fadeOut(600, function () {
        $(this).text(tab[count % tab.length])
                .fadeIn(600);
    });
}, 4500);

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});