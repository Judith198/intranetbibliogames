$("#formValidation").validate({
    rules: {
        contactNom: {
            required: true,
            minlength: 3

        },
        contactPrenom: {
            required: true
        },
        email:{
            email:true
        }
    },

    messages: {
        contactNom: {

            required: "Merci d'entrer votre nom",
            contactNom:"Veuillez saisir votre nom"
        },
        contactPrenom: {
            required: "Merci d'entrer votre prénom",
            contactPrenom:"Veuillez saisir votre prénom"

        },
        email: {
            required: "Merci d'entrer un mail valide",
            email:"Veuillez saisir un mail valide"
        }

    }



}
);