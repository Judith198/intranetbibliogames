<div class="row">
    <div class="col-md-12">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button> <a class="navbar-brand nav-color" href="index.php">Accueil </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="nav-color" href="#"></a>
                    </li>
                    <li>
                        <a class="nav-color" href="#"></a>
                    </li>
                    <li>
                        <a class="nav-color" href="#"></a>
                    </li>
                    <li>
                        <a class="nav-color" href="index.php?gestion="></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav-color" data-toggle="dropdown"><strong class="caret">Recherche</strong></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?gestion=jeux">Jeux</a>
                            </li>

                            <li class="divider"> </li>

                            <li>
                                <a href="index.php?gestion=typeJeux">Type de jeux</a>
                            </li>

                            <li class="divider"> </li>


                            <li>
                                <a href="index.php?gestion=plateforme">Plateformes</a>
                            </li>

                            <li class="divider"> </li>
                            <li>
                                <a href="index.php?gestion=pegiJeux">PEGI</a>
                            </li>

                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="nav-color" href="#">Plan du site</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav-color" data-toggle="dropdown">Espace personnel<strong class="caret"></strong></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">{$deconnexion}</a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="#">Profil</a>

                            </li>

                        </ul>
                    </li>
                </ul>
            </div>

        </nav>
    </div>
</div>