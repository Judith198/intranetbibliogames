<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-13 16:41:45
  from 'C:\wamp64\www\bibliogames\mod_utilisateur\vue\utilisateurFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e949649c4dc76_82735092',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '598f57b6fd48031fd6325101d865ef2eee116bc0' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames\\mod_utilisateur\\vue\\utilisateurFicheVue.tpl',
      1 => 1586796100,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_Administrateur.tpl' => 1,
    'file:public/menu_Moderateur.tpl' => 1,
    'file:public/menu_Utilisateur.tpl' => 1,
    'file:public/piedPage.tpl' => 1,
  ),
),false)) {
function content_5e949649c4dc76_82735092 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\bibliogames\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />

    </head>
    <body>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Administrateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Modérateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Moderateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Utilisateur') {?>
            <?php $_smarty_tpl->_subTemplateRender("file:public/menu_Utilisateur.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <div class="container-fluid mt-5">



            <div class="row">

                <div class=" container-fluid text-center">
                    <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                </div>
                <div class="col-md-2 space">

                </div>
            </div>

            <div class="row mt-5">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    <p <?php if ($_smarty_tpl->tpl_vars['unUtilisateur']->value->getMessageErreur() != '') {?> class="pos-messageErreur" <?php }?>>
                        <?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getMessageErreur();?>

                    </p>
                </div>

            </div>





            <!-- ICI LES DONNES, LE FORMULAIRE (LA FICHE !) -->
            <div class="container col-md-offset-2 col-md-8 col-md-offset-2 space">
                <form action="index.php" method="post" novalidate="">

                    <input type="hidden" name="gestion" value="utilisateur">
                    <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                    <input id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="hidden" value="">

                    <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                        <div class="form-group">
                            <input class="form-control" id="idUtilisateur" name="idUtilisateur" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getIdUtilisateur();?>
" readonly>
                        </div>
                    <?php }?>
                    <div class="form-group">
                        Nom <sup>(*)</sup> :
                        <strong>
                            <input class="form-control" id="nomUtilisateur" name="nomUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getNomUtilisateur();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                        </strong>
                    </div>
                    <div class="form-group">
                        Prenom <sup>(*)</sup> :
                        <strong>
                            <input class="form-control" id="prenomUtilisateur" name="prenomUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getPrenomUtilisateur();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                        </strong>
                    </div>
                    <div class="form-group">
                        Mail de l'utilisateur <sup>(*)</sup>:
                        <input class="form-control" id="mailUtilisateur" name="mailUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getMailUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                    </div>
                    <div class="form-group">
                        Login <sup>(*)</sup> :
                        <input class="form-control" id="loginUtilisateur" name="loginUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getLoginUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                    </div>
                    <div class="form-group">
                        Mot de passe <sup>(*)</sup> :
                        <input class="form-control" id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getMotDePasseUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                    </div>
                    <div class="form-group">
                        Role:
                        <input class="form-control" id="typeUtilisateur" name="typeUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getTypeUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                    </div>

                    <div class="form-group">

                        <div class="col-md-11">
                            <input type="button"  class="btn btn-warning btn-xs"
                                   onclick='location.href = "index.php?gestion=utilisateur"' value="Retour">
                        </div>

                        <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                            <div class="col-md-1">
                                <input type="submit" class="btn btn-warning btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                            </div>
                        <?php }?>

                    </div>

                </form>

            </div>


            <?php $_smarty_tpl->_subTemplateRender('file:public/piedPage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
        <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/custom.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"><?php echo '</script'; ?>
>


    </body>
</html>
<?php }
}
