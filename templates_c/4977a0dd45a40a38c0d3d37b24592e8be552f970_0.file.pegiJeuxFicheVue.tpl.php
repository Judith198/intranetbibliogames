<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-11 12:31:12
  from 'C:\wamp64\www\bibliogames\bibliogames\mod_pegiJeux\vue\pegiJeuxFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e91b890dbd095_82368607',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4977a0dd45a40a38c0d3d37b24592e8be552f970' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames\\bibliogames\\mod_pegiJeux\\vue\\pegiJeuxFicheVue.tpl',
      1 => 1586607100,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu.tpl' => 1,
  ),
),false)) {
function content_5e91b890dbd095_82368607 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\bibliogames\\bibliogames\\include\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

   

        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

            <?php $_smarty_tpl->_subTemplateRender('file:public/menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <div class="row">
                <div class="col-md-4 space">

                </div>

                <div class="col-md-4 space">
               
                </div>

                <div class="col-md-2 space">

                </div>
            </div>

            		<div class="row">
    
            <div class="col-md-offset-2 col-md-8 col-md-offset-2">

 
            </div>	 					




            <div class="row">
                <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                <div class="col-md-offset-2 col-md-8 col-md-offset-2 space">
                    <form action="index.php" method="post" novalidate="">
                           <h1>FICHE PEGI</h1>

                        <input type="hidden" name="gestion" value="pegiJeux">
                        <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">


                        <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                            <div class="form-group ">
                                <label> Identifiant Pegi : </label>
                                <input class="form-control" id="idPegi" name="idPegi" type="text" value="<?php echo $_smarty_tpl->tpl_vars['lePegiJeux']->value->getIdPegi();?>
" readonly>                                </div>
                        <?php }?>
                        <div class="form-group inputGroup-sizing-lg">
                            <label> Pegi :</label>
                            <strong> 
                                <input class="form-control" id="agePegi" name="agePegi" type="text" value="<?php echo $_smarty_tpl->tpl_vars['lePegiJeux']->value->getAgePegi();?>
"  <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                            </strong>
                        </div>

                      

                        <div class="form-group">

                            <div class="col-sm-10">
                                <input type="button"  class="btn btnVert btn-sm"
                                       onclick='location.href = "index.php?gestion=pegiJeux"' value="Retour">
                            </div>

                            <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                <div class="col-md-1">
                                    <input type="submit" class="btn btnVert btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                </div>
                            <?php }?>

                        </div>

                    </form>

                </div>
            </div>

        </div>

        <?php echo '<script'; ?>
 src="public/js/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="public/js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
