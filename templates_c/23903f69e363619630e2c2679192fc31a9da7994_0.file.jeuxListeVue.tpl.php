<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-13 06:06:37
  from 'C:\wamp64\www\bibliogames2\mod_jeux\vue\jeuxListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e94016d01ad80_88068829',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '23903f69e363619630e2c2679192fc31a9da7994' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames2\\mod_jeux\\vue\\jeuxListeVue.tpl',
      1 => 1586757989,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:public/menu_utilisateur.tpl' => 1,
  ),
),false)) {
function content_5e94016d01ad80_88068829 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <!-- Bootstrap core CSS -->
        <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

        <!-- Custom styles for this template -->
        <!-- <link href="css/small-business.css" rel="stylesheet">-->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >


        <link href="public/css/style.css" rel="stylesheet">



    </head>

    <body>

        <!-- Navigation -->

 
        <div class="container-fluid">

            <?php $_smarty_tpl->_subTemplateRender('file:public/menu_utilisateur.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            
            <div class="marge">

            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion=jeux"></a>
                </div>


            </div>

            <!-- Page Content -->
            <div class="container">
                <h1>  Liste des jeux</h1>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeJeux']->value, 'jeux');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['jeux']->value) {
?>
                    <div class="esp"></div>
                    <!-- Heading Row -->
                    <div class="row align-items-center my-5">
                        <div class="col-lg-7">
 
                            <img  class="img-fluid rounded mb-4 mb-lg-0 " src="<?php echo $_smarty_tpl->tpl_vars['jeux']->value['imgJeux'];?>
"  alt="Image du jeu">
                        </div>
                        <!-- /.col-lg-8 -->
                        <div class="col-lg-5">
                            <h2 class="font-weight-light"><strong>  <?php echo $_smarty_tpl->tpl_vars['jeux']->value['nomJeux'];?>
</strong></h2>
                            <p> <?php echo $_smarty_tpl->tpl_vars['jeux']->value['descriptionJeux'];?>
</p>
                            
                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_consulter'>

                                <input type="submit"  class="btn btnVert btn-sm"   name="consulter" value="Consulter">
                            </form>



                            <form action='index.php' method='post'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_ajouter'>
                                <input type="submit"  class="btn btnVert btn-sm"  name="ajouter" value="Ajouter">
                            </form>



                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_modifier'>

                                <input type="submit"  class="btn btnVert btn-sm"   name="modifier" value="Modifier">                                                  
                            </form>

                            <form action='index.php' method='post'>
                                <input type='hidden' name='idJeux' value='<?php echo $_smarty_tpl->tpl_vars['jeux']->value['idJeux'];?>
'>
                                <input type='hidden' name='gestion' value='jeux'>
                                <input type='hidden' name='action' value='form_supprimer'>

                                <input type="submit"  class="btn btnVert btn-sm"   name="supprimer" value="Supprimer">                                                          
                            </form>
                        </div>
                        <!-- /.col-md-4 -->
                    </div>
                    <!-- /.row -->

                <?php
}
} else {
?>
                    <tr>
                        <td colspan='6'>Aucun enregistrement de trouvé.</td>
                    </tr>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        </div>

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; BIBLIOGAMES 2020</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <!-- <?php echo '<script'; ?>
 src="vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>
         <?php echo '<script'; ?>
 src="vendor/bootstrap/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
> -->
        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ><?php echo '</script'; ?>
> 
        <?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"><?php echo '</script'; ?>
> 
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"><?php echo '</script'; ?>
>

    </body>

</html>
<?php }
}
