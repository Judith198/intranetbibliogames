<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-12 08:37:35
  from 'C:\wamp64\www\bibliogames\public\menu_administrateur.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e92d34f713102_89360451',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '86a6231a896eb073bb6d4acdfae190703edf0a36' => 
    array (
      0 => 'C:\\wamp64\\www\\bibliogames\\public\\menu_administrateur.tpl',
      1 => 1586680433,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e92d34f713102_89360451 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-0">
    <div class="container-fluid p-1">
        <a class="navbar-brand" href="index.php?gestion=accueil">Accueil</a>
        
        <div class="input-group d-flex justify-content-end">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Espace personnel
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?gestion=authentification&action=deconnexion"><?php echo $_smarty_tpl->tpl_vars['deconnexion']->value;?>
</a>
                    <a class="dropdown-item" href="index.php?gestion=utilisateur">Utilisateurs</a>
                </div>
            </div>
        </div>

    </div>
</nav><?php }
}
