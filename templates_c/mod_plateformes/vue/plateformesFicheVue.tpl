<!DOCTYPE html>     <
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <link rel="icon" type="image/png" href="public/images/plogo.PNG" />
        <link href="public/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">

    </head>
    <body>

        <div class="container-fluid">

            {include file ='public/menu.tpl'}

            <div class="row">
                <div class="col-md-4 space">

                </div>

                <div class="col-md-4 space">

                </div>

                <div class="col-md-2 space">

                </div>
            </div>

            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                    {*           <p {if $lePegiJeux->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
                    {$lePegiJeux->getMessageErreur()}
                    </p>
                    </div>    *}

                </div>




                <div class="row">
                    <!-- ICI LES DONNEES, LE FORMULAIRE (LA FICHE !) -->
                    <div class="col-md-offset-2 col-md-8 col-md-offset-2 space">
                        <form action="index.php" method="post" novalidate="">
                            <h1>FICHE PLATEFORME</h1>

                            <input type="hidden" name="gestion" value="typeJeux">
                            <input type="hidden" name="action" value="{$action}">


                            {if $action neq 'ajouter'}
                                <div class="form-group ">
                                    <label> Identifiant : </label>
                                    <input class="form-control" id="idPlateforme" name="idPlateforme" type="text" value="{$unePlateforme->getIdPlateforme()}" readonly>    {*unJeu dans jeuxVue.php*}
                                </div>
                            {/if}
                            <div class="form-group inputGroup-sizing-lg">
                                <label> Type de plateforme :</label>
                                <strong>
                                    <input class="form-control" id="nomPlateforme" name="nomPlateforme" type="text" value="{$unePlateforme->getNomPlateforme()}"  {$comportement} required="required">
                                </strong>
                            </div>



                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="button"  class="btn btnVert btn-sm"
                                           onclick='location.href = "index.php?gestion=typeJeux"' value="Retour">
                                </div>

                                {if $action neq 'consulter'}
                                    <div class="col-md-1">
                                        <input type="submit" class="btn btnVert btn-sm" value="{$action|capitalize}">
                                    </div>
                                {/if}

                            </div>

                        </form>

                    </div>
                </div>

            </div>

            <script src="public/js/jquery.min.js"></script>
            <script src="public/js/bootstrap.min.js"></script>
            <script src="public/js/scripts.js"></script>
    </body>
</html>
